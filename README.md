# Bejo map data app

In this project we will prototype the usage of gegraphical/spacial data in the context of the [BejoZaden](https://hal24k.atlassian.net/wiki/spaces/DSP/pages/2730754049/Bejo+Zaden) project.

## Usage
```bash
$ cd your_project_name/src
$ streamlit run app.py
```

## Project Organisation

    ├── chart                        <- Files required for Dimensionops build
    │
    ├── docs                         <- Documentation
    │
    ├── notebooks                    <- Jupyter notebooks
    │
    ├── src                          <- Source code for project
    │   │── setup                    <- Setup
    │   │   │── favicon.ico          <- Icon
    │   │   └── layout.py            <- Layout settings
    │   │
    │   └── app.py                   <- App run file
    │   
    │── Dockerfile                   <- File to assemble a Docker image
    │
    │── environment.yml              <- Environment yml file to create conda environment
    │
    ├── README.md                    <- README for this project
    │
    └── requirements.txt             <- Requirements file for creating app environment