import datetime as dt
from typing import Union

import pytz
import timezonefinder


def get_time_interval(
    datetime: dt.datetime, interval: Union[float, dt.timedelta], lat: float, lon: float
):
    tf = timezonefinder.TimezoneFinder()
    timezone_str = tf.certain_timezone_at(lat=lat, lng=lon)
    tz = pytz.timezone(timezone_str)

    if isinstance(interval, float):
        interval = dt.timedelta(days=interval)

    return tz.localize(datetime - interval), tz.localize(datetime + interval)
