import numpy as np
import pandas as pd
from sklearn import metrics
from sklearn.cluster import DBSCAN
from sklearn.datasets import make_blobs
from sklearn.preprocessing import StandardScaler


def split_data_by_site(df, verbose=False):

    X = df[["Longitude", "Latitude"]].to_numpy()
    X_std = StandardScaler().fit_transform(X)

    db = DBSCAN(eps=0.3, min_samples=2, n_jobs=-1).fit(X_std)
    core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True
    labels = db.labels_

    # Number of clusters in labels, ignoring noise if present.
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    n_noise_ = list(labels).count(-1)

    if verbose:
        print("Estimated number of clusters: %d" % n_clusters_)
        print("Estimated number of noise points: %d" % n_noise_)
        print("Silhouette Coefficient: %0.3f" % metrics.silhouette_score(X, labels))

    # dfs = [df.loc[labels==label] for label in set(labels)]
    df["site_number"] = labels
    return df
