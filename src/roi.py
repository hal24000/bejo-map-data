import datetime as dt
from uuid import UUID, uuid4

import pydantic
import shapely.geometry as shp_geo

from datetime_util import get_time_interval


class ROI(pydantic.BaseModel):
    timestamp: dt.datetime
    GeoJson: dict
    recording_ids: list[str]

    @classmethod
    def from_geometry(
        cls,
        timestamp: dt.datetime,
        geometry: shp_geo.base.BaseGeometry,
        recording_ids: list[str],
    ):
        return cls(
            timestamp=timestamp,
            GeoJson=shp_geo.mapping(geometry),
            recording_ids=recording_ids,
        )

    class Config:
        arbitrary_types_allowed = True

    @property
    def geometry(self):
        # While most of the time we work with the (shapely) geometry and
        # not GeoJson, the latter has the advantage of being serializable
        return shp_geo.shape(self.GeoJson)

    @property
    def wkt(self):
        return self.geometry.wkt

    @property
    def date(self):
        return self.timestamp.date().isoformat()

    def _get_lat_lon(self):
        lon, lat = self.geometry.centroid.xy
        return lat[0], lon[0]

    @property
    def longitude(self):
        _, lon = self._get_lat_lon()
        return lon

    @property
    def latitude(self):
        lat, _ = self._get_lat_lon()
        return lat

    def get_time_interval(self, interval, iso_format=True):

        start_time, end_time = get_time_interval(
            self.timestamp, interval / 2, *self._get_lat_lon()
        )

        if not iso_format:
            return start_time, end_time
        else:
            return start_time.isoformat(), end_time.isoformat()
