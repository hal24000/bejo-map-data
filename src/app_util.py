from collections import defaultdict

import cmasher as cmr
import geopandas as gpd
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import shapely.geometry as shp_geo
import streamlit as st
from streamlit_folium import folium_static

import geodata
import sites


@st.experimental_singleton
def generate_dataframe(crop_name: str = "Carrot", static_data: bool = True):
    df = geodata.prepare_dataframe(task_status=["Done", "Ongoing", "Created"])

    if static_data:
        dset = geodata.IndexedDataset.load("../data/auto_drone_example.json")
        manual_data = geodata.IndexedDataset.load("../data/manual_drone_example.json")
        dset.data += manual_data.data
        dset.name = "Combined manually and auto annotated data from drone footage"

    else:
        dset = geodata.get_dset(crop_name, ["Done", "Ongoing", "Started"])

    df = geodata.include_annotations(df, dset)
    geodata.add_parentlines_and_sex(df, inplace=True)
    geodata.include_time_data(df)

    return df


def color_bar(name: str, categories: list[str], colors: list[str]) -> mpl.figure.Figure:
    """Prepares a figure containing a color bar"""

    cmap = mpl.colors.ListedColormap(colors)
    bounds = np.arange(len(categories) + 1)

    ticks = categories
    fig, ax = plt.subplots(figsize=(10, 0.3))
    norm = mpl.colors.BoundaryNorm(bounds, cmap.N)

    cbar = mpl.colorbar.ColorbarBase(
        cmap=cmap,
        ax=ax,
        norm=norm,
        boundaries=bounds,  # Adding values for extensions.
        ticks=bounds[:-1] + 0.5,
        spacing="uniform",
        orientation="horizontal",
        label=name,
    )
    cbar.ax.set_xticklabels(ticks)
    return fig


def color_by(df: gpd.GeoDataFrame, name: str) -> tuple[list[str], mpl.figure.Figure]:
    """
    Produces a list of colors to help colouring a folium map
    and an associated colorbar

    Parameters
    ----------
    df
        (Geo)Pandas dataframe containing the BBCH counts (in the BBCH column)
        parentline or any other quantity
        and positions as points in the geometry column
    name
        Column name which will be used to colour-code the data

    Returns
    -------
    colors_list
        A list of strings containing hex codes of the colours
    fig
        Matplotlib figure with the associated colorbar
    """
    categories = df[name].unique()

    if name == "BBCH":
        cmap = "cmr.chroma"
        categories = np.arange(50, 71)
    elif name == "parentline":
        cmap = "Paired"
    else:
        cmap = "cmr.tree"
    colors = cmr.take_cmap_colors(
        cmap, len(categories), cmap_range=(0.15, 0.85), return_fmt="hex"
    )
    if name == "sex":
        colors = ["darkred", "darkblue"]

    color_mapping = {cat: color for cat, color in zip(categories, colors)}

    fig = color_bar(name, categories, colors)

    return [color_mapping[category] for category in df[name]], fig


def aggregate_plots(df: gpd.GeoDataFrame, buffer: float = 1) -> gpd.GeoDataFrame:
    """
    Combines data from individual recordings

    Parameters
    ----------
    df
        GeoPandas dataframe containing the BBCH counts for individual images
        and positions as points in the geometry column
    buffer
        Size of the buffer zone (in meters) attached to each point

    Returns
    -------
    aggregated_df
        GeoDataFrame containing aggregated individual recordings as rows
    """
    BBCH_columns = sorted(c for c in df.columns if "BBCH_" in c)

    aggregated_data = defaultdict(list)

    # Projects into cartesian coordinates to allow
    # operations in meters
    df = geodata.make_cartesian(df)

    for recording_id, sub_df in df.groupby("recording_id"):

        for c in ["parentline", "sex", "site_number", "timestamp", "plot_number"]:
            assert len(sub_df[c].unique()) == 1
            aggregated_data[c].append(sub_df.iloc[0][c])

        aggregated_data["recording_id"].append(recording_id)

        # Generate aggregated histogram
        aggregated_hist = sub_df[BBCH_columns].sum(axis=0)
        aggregated_hist /= aggregated_hist.sum()
        aggregated_data["histogram"].append(aggregated_hist.to_numpy())

        # Useful extras
        max_idx = aggregated_hist.argmax()
        aggregated_data["BBCH"].append(int(BBCH_columns[max_idx].replace("BBCH_", "")))
        aggregated_data["percentage"].append(aggregated_hist[max_idx])

        # Creates a line from the plots positions
        aggregated_data["geometry"].append(shp_geo.MultiPoint(list(sub_df.geometry)))

    aggregated_df = gpd.GeoDataFrame(aggregated_data).set_crs(df.crs)
    aggregated_df["timestamp"] = aggregated_df.timestamp.apply(lambda x: str(x))
    aggregated_df["geometry"] = aggregated_df.geometry.buffer(buffer)

    return aggregated_df.to_crs(epsg=4326)
