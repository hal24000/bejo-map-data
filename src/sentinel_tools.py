import collections
import datetime as dt
import io
import os
import re
import zipfile
from abc import ABCMeta, abstractmethod
from typing import Any, Literal, Union

import large_image
import netCDF4 as nc
import numpy as np
import pydantic
from scipy.interpolate import griddata
from sentinelsat import SentinelAPI
from sentinelsat.exceptions import LTATriggered

import geodata
from roi import ROI


class SentinelData(pydantic.BaseModel, metaclass=ABCMeta):
    """Represents downloaded Sentinel data"""

    roi_list: list[ROI] = pydantic.Field(default_factory=list)
    """List of RoIs which are known to be in this data"""
    data_path: str
    """Path to the directory with the images and metadata"""
    product_id: str
    """Product id associated with the data"""
    product_type: str
    """Product type associated with the data"""
    resolution: str
    """Spatial resolution of the data"""

    class Config:
        underscore_attrs_are_private = True

    @abstractmethod
    def get_path(self, band_code: str):
        """Fetches the path where the image is stored"""
        pass

    @abstractmethod
    def get(
        self, band_code: str, buffer: float = 200, width: int = 250
    ) -> tuple[np.ndarray, tuple[float]]:
        """
        Fetches a pre-cropped numpy array containing the data

        A rectangle parallel to the (lon,lat) coordinate axis encompassing
        all the RoIs in :py:data:`SentinelData.roi_list` plus an specified
        buffer around them is constructed. This is used to crop the selected
        image/band. The result is returned as a :py:obj:`numpy.ndarray`
        together with the bounds. The new array corresponds to the values of
        the selected quantity interpolated/reprojected on a uniform
        grid of longitudes and latitudes.

        Parameters
        ----------
        band_code
            String used to designate a specific band
        buffer
            Size of the buffer zone around the RoIs which will be included
            in the crop
        width
            Number of columns in the output array

        Returns
        -------
        arr
            Cropped output array
        bounds
            A tuple containing the (lon_min, lat_min, lon_max, lat_max) bounds of
            the cropped region
        """
        pass

    @abstractmethod
    def available_bands(self, description: bool):
        pass


class Sentinel2Data(SentinelData):
    resolution: Literal["10m", "20m", "60m"] = "10m"

    def check_resolution(self):
        # Unfortunatelly there is no way of making a pydantic validator
        # which refers to the instance instead of the class
        assert (
            self.resolution in self.available_resolutions()
        ), "Selected resolution is invalid. It must be one of: " + ", ".join(
            self.available_resolutions()
        )

    @property
    def images_path(self):
        granules = os.listdir(os.path.join(self.data_path, "GRANULE"))

        if len(granules) > 1:
            print("Something strange.. there is more than one granule")

        return os.path.join(self.data_path, "GRANULE", granules[0], "IMG_DATA")

    def available_resolutions(self):
        # Finds resolutions from directory structure
        return sorted([d.replace("R", "") for d in os.listdir(self.images_path)])

    def available_bands(self, description: bool = False):
        # Validates resolution
        self.check_resolution()

        # Finds bands from filenames
        res_img_path = os.path.join(self.images_path, "R" + self.resolution)

        bands = []
        for filename in os.listdir(res_img_path):
            if match := re.match(r".+_(\w\w\w)_\d\dm\.jp2$", filename):
                bands.append(match.group(1))

        return sorted(bands)

    def get_path(self, band_code: str):
        assert (
            band_code in self.available_bands()
        ), f"band_code must be one of: {', '.join(self.available_bands())}"

        res_img_path = os.path.join(self.images_path, "R" + self.resolution)
        for filename in os.listdir(res_img_path):
            if band_code in filename and filename[-3:] == "jp2":
                break

        image_path = os.path.join(res_img_path, filename)

        return image_path

    def get(self, band_code: str, buffer: float = 200, width: int = 250) -> np.ndarray:
        limg = large_image.open(self.get_path(band_code))

        lon_min, lat_min, lon_max, lat_max = geodata.get_crop_coords(
            self.roi_list, buffer=buffer
        )

        arr, _ = limg.getRegion(
            region={
                "left": lon_min,
                "right": lon_max,
                "bottom": lat_min,
                "top": lat_max,
                "units": "EPSG:4326",
            },
            output=dict(maxWidth=width),
            format=large_image.constants.TILE_FORMAT_NUMPY,
        )

        # Workaround for an apparent bug in large_image
        # (by default the array is generated with 4 channels
        # with the extra channels filled with 255
        n_channels = len(limg.getMetadata()["bands"])

        arr = arr[:, :, :n_channels]

        return arr, (lon_min, lat_min, lon_max, lat_max)


class Sentinel3Data(SentinelData):
    resolution: Literal["300m", "1.2km"] = "300m"

    _latitude: Any = None
    _longitude: Any = None
    _netCDFfiles: dict = {}
    _supported_bands_L2 = {
        "IWV": (
            "Integrated Water Vapour. Total amount of water vapour integrated over an atmosphere column. kg*m^-2",
            "iwv",
        ),
        "FAPAR": (
            "Fraction of Absorbed Photosynthetically Active Radiation (FAPAR) in the plant canopy (aka OGVI or OLCI Global Vegetation Index)",
            "ogvi",
        ),
        "RC_OGVI": (
            "By-products of the OGVI, red and NIR rectified reflectances, are virtual reflectance largely decontaminated from atmospheric and angular effects, and good proxy to top of canopy reflectances.",
            "rc_ogvi",
        ),
        "TCI": (
            "Terrestrial Chlorophyll Index. Estimates of the chlorophyll content in terrestrial vegetation (aimed at monitoring vegetation condition and health).",
            "otci",
        ),
    }

    def __init__(self, **data):
        super().__init__(**data)

        for f in os.listdir(self.data_path):
            if ".nc" not in f:
                continue

            fpath = os.path.join(self.data_path, f)
            key = f.replace(".nc", "")
            self._netCDFfiles[key] = nc.Dataset(fpath, "r")

    @property
    def latitude_grid(self):
        if self._latitude is None:
            self.latitude = nc.Dataset(fpath, "r")

    def available_bands(self, descriptions: bool = False):

        if not descriptions:
            return list(self._supported_bands_L2.keys())
        else:
            return {k: v[0] for k, v in self._supported_bands_L2.items()}

    def get_path(band_code: str):
        return self._netCDFfiles[band_code]

    def get(
        self, band_code: str, buffer: float = 200, width=250, method="nearest"
    ) -> np.ndarray:

        # Shorthand variables exploiting the structure of the netCDF files
        key = self._supported_bands_L2[band_code][1]
        quantity = self._netCDFfiles[key].variables[key.upper()]
        lat = self._netCDFfiles["geo_coordinates"].variables["latitude"]
        lon = self._netCDFfiles["geo_coordinates"].variables["longitude"]

        lon_min, lat_min, lon_max, lat_max = geodata.get_crop_coords(
            self.roi_list, buffer=buffer
        )

        # Computes interpolating grid size, keeping aspect ratio
        nx = width
        ny = abs(int(nx * (lat_max - lat_min) / (lon_max - lon_min)))

        # Creates a latitude-longitude regular grid for output
        grid_lon, grid_lat = np.mgrid[
            lon_min : lon_max : nx * 1j, lat_min : lat_max : ny * 1j
        ]

        # Reads the mask and the full data from the NetCDF files
        # Note: this can be quite memory intensive
        data = quantity[:, :].data
        mask = ~quantity[:, :].mask
        # The mask is reversed, so that it can be used as a filter

        # Now we interpolate into a new grid which is regularly spaced
        # along latitudes and longitudes
        cropped_grid = griddata(
            (lon[:, :][mask].ravel(), lat[:, :][mask].ravel()),
            data[mask].ravel(),
            (grid_lon, grid_lat),
            method=method,
        )

        return cropped_grid, (lon_min, lat_min, lon_max, lat_max)


class SentinelSession:
    def __init__(
        self,
        rois: Union[ROI, list[ROI]],
        copernicus_username: Union[str, None] = None,
        copernicus_password: Union[str, None] = None,
        data_dir: str = "data",
        product_type: str = "S2MSI2A",
        max_cloud_cover: float = 33,
        max_days_interval: float = 10,
        auto_fetch: bool = True,
        fetch_only_available: bool = False,
        **kwargs,
    ):
        if copernicus_username is None:
            copernicus_username = os.environ["COPERNICUS_USER"]
        if copernicus_password is None:
            copernicus_password = os.environ["COPERNICUS_PASSWORD"]

        if isinstance(rois, list):
            self.ROIs = rois
        else:
            self.ROIs = [rois]

        self.sentinel_api = SentinelAPI(copernicus_username, copernicus_password)

        self.data_dir = data_dir
        self.product_type = product_type
        self.max_cloud_cover = max_cloud_cover
        self.max_days_interval = max_days_interval
        self.fetch_only_available = fetch_only_available

        self.item_ids = []
        self.products_info = []
        for roi in self.ROIs:
            item_id, product_info = self.get_closest_product(roi, **kwargs)
            self.item_ids.append(item_id)
            self.products_info.append(product_info)

        # Creates an auxiliary dictionary to later tackle the
        # case of multiple ROIs being covered by the same image
        self.item_roi_dict = collections.defaultdict(list)
        for roi, item_id in zip(self.ROIs, self.item_ids):
            self.item_roi_dict[item_id].append(roi)

        if auto_fetch:
            self.fetch()
        else:
            self.data = [None] * len(self.item_ids)

    def fetch(self):

        # Different directory conventions apply to different products
        if "S2MSI" in self.product_type:
            suffix = ".SAFE"
        elif "OL_2_" in self.product_type:
            suffix = ".SEN3"
        else:
            raise ValueError(f"{self.product_type} not supported (yet)")

        missing_items = []
        product_paths = []

        for item_id, product in zip(self.item_ids, self.products_info):
            if item_id is None:
                continue

            product_path = os.path.join(self.data_dir, product["title"])
            product_path += suffix

            product_paths.append(product_path)

            if not os.path.isdir(product_path):
                missing_items.append(item_id)

        LTA_list = []
        if len(missing_items) > 0:
            if not self.fetch_only_available:
                # Will try to download everything, waiting for availability of anything
                # in the long term archive
                self.sentinel_api.download_all(
                    missing_items, directory_path=self.data_dir
                )
            else:
                # Will download only what is available, and trigger activation of
                # items in the long term archive (LTA)
                for item_id in missing_items:
                    try:
                        self.sentinel_api.download(
                            item_id, directory_path=self.data_dir
                        )
                    except Exception as err:
                        print(f"Skipping {item_id}")
                        print(f"{type(err).__name__} was raised: {err}")
                        LTA_list.append(item_id)

        for product_path in product_paths:
            zip_path = product_path.replace(suffix, ".zip")

            if os.path.isfile(zip_path):
                with zipfile.ZipFile(zip_path, "r") as zip_ref:
                    zip_ref.extractall(self.data_dir)

                if os.path.isdir(product_path):
                    os.remove(zip_path)

        self.data = [
            self.make_data_object(item_id, product_path)
            for item_id, product_path in zip(self.item_ids, product_paths)
            if (item_id not in LTA_list) and item_id is not None
        ]

    def make_data_object(self, item_id, product_path, resolution: str = "highest"):

        if self.product_type == "S2MSI2A":
            if resolution == "highest":
                resolution = "10m"

            return Sentinel2Data(
                resolution=resolution,
                product_id=item_id,
                product_type=self.product_type,
                data_path=product_path,
                roi_list=self.item_roi_dict[item_id],
            )
        elif self.product_type == "OL_2_LFR___":
            return Sentinel3Data(
                resolution="300m",
                product_id=item_id,
                product_type=self.product_type,
                data_path=product_path,
                roi_list=self.item_roi_dict[item_id],
            )
        elif self.product_type == "OL_2_LRR___":
            return Sentinel3Data(
                resolution="1.2km",
                product_id=item_id,
                product_type=self.product_type,
                data_path=product_path,
                roi_list=self.item_roi_dict[item_id],
            )
        else:
            return product_path

    def get_closest_product(self, roi, **kwargs):

        # Gradually increments the number of days in the query until there is any product
        for days in range(2, self.max_days_interval + 1):
            products = self.sentinel_api.query(
                roi.wkt,
                producttype=self.product_type,
                date=[roi.timestamp + dt.timedelta(days=h) for h in (-days, +days)],
                limit=10,
                cloudcoverpercentage=f"[0 TO {self.max_cloud_cover}]",
                **kwargs,
            )
            if len(products) > 0:
                break

        if len(products) == 0:
            return None, {}

        # Gets the closest product
        delta = dt.timedelta(days=self.max_days_interval)
        for product_id, product in products.items():
            new_delta = abs(roi.timestamp - product["beginposition"])

            if new_delta < delta:
                delta = new_delta
                selected_product = product
                selected_id = product_id

        return selected_id, selected_product
