import random
import re
from collections import Counter

import bejo
import cvwb
import geopandas as gpd
import numpy as np
import pandas as pd
import shapely.geometry as shp_geo
import utm
from pyproj import CRS
from tqdm.autonotebook import tqdm

import sites

pi_2 = np.pi / 2  # shorthand


def prepare_dataframe(
    relevant_metadata=[
        "AbsoluteAltitude",
        "RelativeAltitude",
        "GPSLatitude",
        "GPSLongitude",
        "CameraPitch",
        "CameraYaw",
        "CameraRoll",
        "FOV",
        "ImageWidth",
        "ImageHeight",
        "DateTimeOriginal",
    ],
    renaming_dict={
        "AbsoluteAltitude": "Altitude",
        "RelativeAltitude": "Height",
        "GPSLatitude": "Latitude",
        "GPSLongitude": "Longitude",
        "CameraPitch": "Pitch",
        "CameraYaw": "Yaw",
        "CameraRoll": "Roll",
        "DateTimeOriginal": "Timestamp",
    },
    task_status=["Done"],
):
    """
    Prepares a dataframe containing data about drone images in the Bejo tenant.
    """
    # using the CVWB package to access the correct database
    db = cvwb.data.database.get_database("BEJO")

    drone_recordings_pipeline = [
        # # Selects the right job
        # {'$match': {'cvat_job_id': job_number}},
        # Finds the annotation task entry
        {
            "$lookup": {
                "from": "vpm_annotation_tasks",
                "localField": "annotation_task_id",
                "foreignField": "_id",
                "as": "task",
            }
        },
        # Opens the task details
        {"$unwind": "$task"},
        # Reads the recording id and task name
        {
            "$replaceRoot": {
                "newRoot": {
                    "recording_id": "$task.recording_id",
                    "task_name": "$task.name",
                    "job_id": "$cvat_job_id",
                    "task_status": "$task.cvat_status",
                }
            }
        },
        # Filters by task name (gets the drone footages)
        {"$match": {"task_name": {"$regex": "drone", "$options": "i"}}},
        # Filters by status
        {"$match": {"task_status": {"$in": task_status}}},
    ]

    drone_recordings = list(db.vpm_annotation_jobs.aggregate(drone_recordings_pipeline))

    # We will construct a list of dfs, each containing data related
    # to a single recording id (this may be simplified later...)
    dfs = []

    # This will be used select project the metadata in the database
    relevant_metadata_dict = {
        m: "$metadata.cam_details." + m for m in relevant_metadata
    }

    # Loops over recordings
    # TODO: perhaps this could be integrated to the previous pipeline
    for recording_dict in drone_recordings:
        recording_id = recording_dict["recording_id"]

        pipeline = [
            {"$match": {"recording_id": recording_id}},
            {
                "$replaceRoot": {
                    "newRoot": {
                        "recording_id": "$recording_id",
                        "frame": "$frame_start_number",
                        "filename": "$file_name",
                    }
                    | relevant_metadata_dict
                }
            },
            {
                "$lookup": {
                    "from": "vpm_frames",
                    "localField": "recording_id",
                    "foreignField": "recording_id",
                    "as": "frame_data",
                }
            },
            {
                "$project": {
                    "frame_data": {
                        "$filter": {
                            "input": "$frame_data",
                            "as": "frame_data",
                            "cond": {
                                "$eq": ["$$frame_data.frame_nr_recording", "$frame"]
                            },
                        }
                    },
                    "recording_id": 1,
                    "frame": 1,
                    "filename": 1,
                }
                | {k: 1 for k in relevant_metadata}
            },
            {"$unwind": {"path": "$frame_data"}},
            {
                "$replaceRoot": {
                    "newRoot": {
                        "recording_id": "$recording_id",
                        "frame_id": "$frame_data._id",
                        "filename": "$filename",
                        "blob_path_high": "$frame_data.file_path.high",
                        "blob_path_low": "$frame_data.file_path.low",
                    }
                    | {k: "$" + k for k in relevant_metadata if k not in renaming_dict}
                    | {v: "$" + k for k, v in renaming_dict.items()}
                }
            },
        ]

        df = pd.DataFrame(list(db.vpm_videos.aggregate(pipeline)))
        df["task_name"] = recording_dict["task_name"]
        df["job_id"] = recording_dict["job_id"]

        for c in (
            "Altitude",
            "Height",
            "Latitude",
            "Longitude",
            "Pitch",
            "Yaw",
            "Roll",
            "ImageWidth",
            "ImageHeight",
        ):
            if c in df:
                df[c] = pd.to_numeric(df[c])

        df["frame_id"] = [str(frame_id) for frame_id in df.frame_id]

        if "FOV" in df:
            df.FOV = df.FOV.map(lambda x: float(re.sub(r"(\d+\.?\d+).+", r"\1", x)))

        if "Timestamp" in df:
            df.Timestamp = df.Timestamp.map(
                lambda timestamp: pd.to_datetime(
                    re.sub(
                        r"(\d\d\d\d):(\d\d):(\d\d) (\d\d:\d\d:\d\d)",
                        r"\1-\2-\3 \4",
                        timestamp,
                    )
                ).isoformat()
            )

        dfs.append(df)
    # Joins all the recordings
    df = pd.concat(dfs).reset_index()

    # Includes the column which automatically groups close points
    df = sites.split_data_by_site(df)

    # Converts to a GeoPandas DataFrame
    df = gpd.GeoDataFrame(
        df, geometry=gpd.points_from_xy(x=df.Longitude, y=df.Latitude)
    ).set_crs(epsg=4326)

    return df


class IndexedDataset(cvwb.data.Dataset):
    """
    Extension to the :py:class:`cvwb.data.Dataset` with a convenient frame_id_idx
    property
    """

    @property
    def frame_id_idx(self):
        if "frame_id_idx" not in self._cache:
            self._cache["frame_id_idx"] = {img.frame_id: img for img in self}
        return self._cache["frame_id_idx"]

    @classmethod
    def from_dataset(cls, dset: cvwb.data.Dataset):
        return cls(
            data=dset.data,
            name=dset.name,
            id=dset.id,
            creation_date=dset.creation_date,
            notes=dset.notes,
        )


def add_parentlines_and_sex(
    df, possible_parentlines=["Sw", "Berduci", "Nabci", "Mok"], inplace=False
):

    parentlines = []
    sexes = []

    for task in df.task_name:

        for p in possible_parentlines:
            if match := re.search(f"({p} ([ABC])\d?.?\d?)", task):
                parentline = match.group(1)
                sex = match.group(2)
                break
            else:
                parentline = sex = None
        sexes.append(sex)
        parentlines.append(parentline)

    if not inplace:
        df = df.copy(deep=True)

    df["sex"] = sexes
    df["parentline"] = parentlines

    return df


def include_time_data(df):
    db = cvwb.data.database.get_database("BEJO")

    recording_ids = list(df.recording_id)
    pipeline = [
        {"$match": {"recording_id": {"$in": recording_ids}}},
        {
            "$replaceRoot": {
                "newRoot": {
                    "recording_id": "$recording_id",
                    "Timestamp": "$metadata.cam_details.DateTimeOriginal",
                }
            }
        },
    ]
    timestamps = {}
    for record in db.vpm_videos.aggregate(pipeline):
        if "Timestamp" in record:
            timestamp = record["Timestamp"]

            timestamp = re.sub(
                r"(\d\d\d\d):(\d\d):(\d\d) (\d\d:\d\d:\d\d)",
                r"\1-\2-\3 \4",
                timestamp,
            )

        else:
            timestamp = np.nan

        timestamps[record["recording_id"]] = timestamp

    df["timestamp"] = [pd.to_datetime(timestamps[rec_id]) for rec_id in df.recording_id]

    return df


def get_dset(crop_name="Carrot", task_status=["Done", "Ongoing", "Started"]):
    """
    Loads a dataset containing annotated drone footage for Bejo
    """
    dset = bejo.data.fetch_dataset(
        crop_name=crop_name,
        footage_type="drone",
        remove_annotationless=False,
        task_status=task_status,
    )

    dset = bejo.data.construct_cv_labels(crop_name=crop_name, dset=dset)

    dset = IndexedDataset.from_dataset(dset)

    return dset


def include_annotations(
    df: pd.DataFrame, dset: IndexedDataset, min_BBCH: int = 50, max_BBCH: int = 71
):
    """
    Includes the cv label data from a cvwb Dataset into a data frame
    containing frame_id information
    """
    base_dict = {f"BBCH_{i}": 0 for i in range(min_BBCH, max_BBCH + 1)}

    annotation_data = []
    for frameid in tqdm(df.frame_id):
        d = base_dict.copy()
        d["max_count"] = 0
        d["max_BBCH"] = pd.NA

        if frameid in dset.frame_id_idx:
            c = Counter(
                [
                    f"BBCH_{ann.cv_label}"
                    for ann in dset.frame_id_idx[frameid].annotations
                ]
            )
            d |= c

            if len(c) > 0:
                max_BBCH, max_count = c.most_common(1)[0]
                d["max_count"] = max_count
                d["max_BBCH"] = int(max_BBCH.replace("BBCH_", ""))

        annotation_data.append(d)

    df = df.join(pd.DataFrame(annotation_data))

    return df


def get_epgs_code(lat, lon):
    crs = CRS.from_dict(
        {"proj": "utm", "zone": utm.from_latlon(lat, lon)[2], "south": lat < 0}
    )
    return int(crs.to_authority()[1])


def project_corners(d):
    # Adjusts Field of View
    diagonal = np.sqrt(d.ImageWidth ** 2 + d.ImageHeight ** 2)
    FOV_x = d.FOV * d.ImageWidth / diagonal
    FOV_y = d.FOV * d.ImageHeight / diagonal

    # Find the centre of the image relative to the drone
    x_c = d.Height * np.tan(pi_2 - (-d.Pitch))
    y_c = d.Height * np.tan(pi_2 - (-d.Pitch))

    # Finds the corners of the image
    x_min = x_c + d.Height * np.tan(pi_2 - (-d.Pitch + FOV_x / 2))
    y_min = y_c + d.Height * np.tan(pi_2 - (-d.Pitch + FOV_y / 2))

    x_max = x_c + d.Height * np.tan(pi_2 - (-d.Pitch - FOV_x / 2))
    y_max = y_c + d.Height * np.tan(pi_2 - (-d.Pitch - FOV_y / 2))

    # Rotates (accounting for the heading/yaw of the drone)
    x_min, y_min = (
        x_min * np.cos(d.Yaw) - y_min * np.sin(d.Yaw),
        x_min * np.sin(d.Yaw) + y_min * np.cos(d.Yaw),
    )
    x_max, y_max = (
        x_max * np.cos(d.Yaw) - y_max * np.sin(d.Yaw),
        x_max * np.sin(d.Yaw) + y_max * np.cos(d.Yaw),
    )

    # Extracts the projected coordinates of the drone
    x_drone, y_drone = d.geometry.x, d.geometry.y

    x_min += x_drone
    x_max += x_drone
    y_min += y_drone
    y_max += y_drone

    return x_min, x_max, y_min, y_max


def generate_polygon(x_min, x_max, y_min, y_max):
    return shp_geo.Polygon(
        [(x_min, y_min), (x_min, y_max), (x_max, y_max), (x_max, y_min)]
    )


def make_cartesian(dataframe):
    # Prepares a (new) dataframe in the correct projection
    epsg_code = get_epgs_code(dataframe.iloc[0].Latitude, dataframe.iloc[0].Longitude)
    return dataframe.to_crs(epsg=epsg_code)


def construct_areas(dataframe):
    # Prepares a (new) dataframe in the correct projection
    df = make_cartesian(dataframe)

    # Adjusts the angles
    for angle in ("Pitch", "Yaw", "Roll", "FOV"):
        df[angle] = np.deg2rad(df[angle])

    # Vectorized part
    corners = project_corners(df)

    # Sequential part
    polygons = [
        generate_polygon(x_min, x_max, y_min, y_max)
        for x_min, x_max, y_min, y_max in zip(*corners)
    ]

    df.geometry = polygons

    return df


def get_crop_coords(ROIs, buffer=200, output_epsg=4326):
    # Prepares a GeoDataFRame (assumed ROIs always use mercator)
    roi_data = gpd.GeoDataFrame([{"geometry": roi.geometry} for roi in ROIs]).set_crs(
        epsg=4326
    )

    # Gets central latitude and longitude
    lon, lat = shp_geo.MultiPolygon(x for x in roi_data.geometry).centroid.xy

    # Projects to UTM to be able to manipulate the geometry in meters
    roi_data = roi_data.to_crs(epsg=get_epgs_code(lon=lon[0], lat=lat[0]))

    # Creates a buffer (in meters) around the original points
    roi_data["geometry"] = roi_data.geometry.buffer(buffer)

    # Finds a rectangle (parallel to the coordinate axes) that contains
    # the points+buffer
    region = gpd.GeoDataFrame(
        {"geometry": [shp_geo.MultiPolygon(x for x in roi_data.geometry).envelope]},
        crs=roi_data.crs,
    )

    # Converts back to latitudes/longitudes (or some other epsg)
    region = region.to_crs(epsg=output_epsg)

    return region.geometry[0].bounds
