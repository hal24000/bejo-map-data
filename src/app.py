from dotenv import load_dotenv

load_dotenv()

import datetime as dt
import os

import bejo
import folium
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import shapely.geometry as shp_geo
import streamlit as st
from streamlit_folium import folium_static

import geodata
import sentinel_tools
import sites
from app_util import aggregate_plots, color_bar, color_by, generate_dataframe
from planet_adaptor import PlanetSession
from roi import ROI

os.environ[
    "LOCALTILESERVER_CLIENT_PREFIX"
] = f"{os.environ['JUPYTERHUB_SERVICE_PREFIX']}/proxy/{{port}}"

from folium import Map
from localtileserver import TileClient, get_folium_tile_layer

st.set_page_config(layout="wide")

# Adds logo to side bar
with st.sidebar:
    _, col_centre, _ = st.columns([2, 2, 3])
    with col_centre:
        st.image("HAL24K-agri-logo.png", width=95)
    st.markdown("---")


# Generates the geodataframe
df = generate_dataframe()


def fetch_sentinel_data(df, selected_site_number=None, fetch_only_available=True):
    df["date"] = [str(t.to_pydatetime().date()) for t in df["timestamp"]]

    ROIs = []
    for date, date_df in df.groupby("date"):
        for site_number, sub_df in date_df.groupby("site_number"):
            if selected_site_number is not None:
                if site_number != selected_site_number:
                    continue

            geometry = shp_geo.MultiPoint(
                list(sub_df.geometry)
            ).minimum_rotated_rectangle

            ROIs.append(
                ROI.from_geometry(
                    timestamp=sub_df.timestamp.mean(),
                    geometry=geometry,
                    recording_ids=list(sub_df.recording_id.unique()),
                )
            )

    sentinel = sentinel_tools.SentinelSession(
        ROIs,
        product_type="S2MSI2A",
        max_days_interval=10,
        data_dir="../data/",
        fetch_only_available=fetch_only_available,
        max_cloud_cover=10,
    )
    return sentinel.data


# sentinel_data = fetch_sentinel_data(df, fetch_only_available=False)

# Includes plot number information
plots_dict = {rec: i + 1 for i, rec in enumerate(sorted(df.recording_id.unique()))}
df["plot_number"] = df.recording_id.apply(lambda x: plots_dict[x])

# Date slider
if st.sidebar.checkbox("Filter by date", value=True):
    df["date"] = [t.date() for t in df.timestamp]
    selected_date = st.select_slider("Choose a date", sorted(df.date.unique()))

    df_selected = df[df.date == selected_date].copy(deep=True)
else:
    df_selected = df.copy(deep=True)


# Aggregates selected data
aggregated_df = aggregate_plots(df_selected)

# Folium will only work with pickeable columns
valid_columns = [
    c for c in sorted(aggregated_df.columns) if c not in ("geometry", "histogram")
]

# Chooses the colour code and which site/parcel to show
color_choices = ["plot number", "BBCH", "parentline", "sex"]
with st.sidebar:
    color_choice = st.radio("Color by", color_choices).replace(" ", "_")

    i_site = (
        st.radio(
            "Center map on parcel:", range(1, len(df_selected.site_number.unique()) + 1)
        )
        - 1
    )

    site_lat = df[df.site_number == i_site].Latitude.mean()
    site_lon = df[df.site_number == i_site].Longitude.mean()


# Gets the colours list for the above selection and the associated colorbar
colors, colorbar_fig = color_by(aggregated_df, color_choice)

# Prepares the main map
folium_map = aggregated_df[valid_columns + ["geometry"]].explore(
    max_zoom=25,
    location=[site_lat, site_lon],
    zoom_start=18,
    color=colors,
    tiles="https://mt1.google.com/vt/lyrs=s&x={x}&y={y}&z={z}",
    attr="Google",
    name="Google Satellite",
)

# Constructs ROIs for image loading
ROIs = []

sentinel_data = fetch_sentinel_data(df_selected, selected_site_number=i_site)


for d in sentinel_data:
    # First, create a tile server from local raster file
    print(d.get_path("TCI"))
    client = TileClient(d.get_path("TCI"))

    # Create folium tile layer from that server
    t = get_folium_tile_layer(
        client,
        overlay=True,
        control=True,
        attr="ESA",
        name="Sentinel 2 – " + d.roi_list[0].date,
    )
    t.add_to(folium_map)

# for roi in ROIs:
#     planet = PlanetSession(roi)
#     basemap = folium.TileLayer(
#         tiles=planet.get_tile_url(generic=True, proc='ndvi'),
#         attr="Planet",
#         name=planet.date,
#         overlay=True,
#         control=True,
#     )
#     basemap.add_to(folium_map)


# basemaps['Google Maps'].add_to(folium_map)


# basemaps = {
#     'Google Maps': folium.TileLayer(
#         tiles = 'https://mt1.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',
#         attr = 'Google',
#         name = 'Google Maps',
#         overlay = True,
#         control = True
#     ),
#     'Google Satellite': folium.TileLayer(
#         tiles = 'https://mt1.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',
#         attr = 'Google',
#         name = 'Google Satellite',
#         overlay = True,
#         control = True
#     ),
#     'Google Terrain': folium.TileLayer(
#         tiles = 'https://mt1.google.com/vt/lyrs=p&x={x}&y={y}&z={z}',
#         attr = 'Google',
#         name = 'Google Terrain',
#         overlay = True,
#         control = True
#     ),
#     'Google Satellite Hybrid': folium.TileLayer(
#         tiles = 'https://mt1.google.com/vt/lyrs=y&x={x}&y={y}&z={z}',
#         attr = 'Google',
#         name = 'Google Satellite',
#         overlay = True,
#         control = True
#     ),
#     'Esri Satellite': folium.TileLayer(
#         tiles = 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
#         attr = 'Esri',
#         name = 'Esri Satellite',
#         overlay = True,
#         control = True
#     )
# }

# basemaps['Google Maps'].add_to(folium_map)

# basemaps['Google Satellite Hybrid'].add_to(folium_map)
# basemaps['Esri Satellite'].add_to(folium_map)


# # Add a layer control panel to the map.
folium_map.add_child(folium.LayerControl())

col1, col2 = st.columns([9, 4])
with col1:
    # Shows the map and the colorbar
    folium_static(folium_map)
    st.pyplot(colorbar_fig)

# ----------------------
# Histograms preparation
# ----------------------

# Common x-axis extracted from df
x = sorted(int(c.replace("BBCH_", "")) for c in df.columns if "BBCH_" in c)

large_number_of_plots = (
    len(aggregated_df[aggregated_df.site_number == i_site].plot_number) > 5
)

with col2:
    if large_number_of_plots:
        plots = ", ".join(
            [
                str(p)
                for p in aggregated_df[aggregated_df.site_number == i_site].plot_number
            ]
        )

        # st.write(f"Available plots: {plots}")
        plot_number = st.select_slider(
            "Pick a plot number",
            aggregated_df[aggregated_df.site_number == i_site].plot_number,
        )
        selection = aggregated_df[aggregated_df.plot_number == plot_number]
    else:
        selection = aggregated_df[aggregated_df.site_number == i_site]

    for plot_number, hist, sex, timestamp in zip(
        selection.plot_number, selection.histogram, selection.sex, selection.timestamp
    ):

        fig, ax = plt.subplots(figsize=(3, 2))
        if not large_number_of_plots:
            plt.title(f"Plot {plot_number}")
        else:
            date = dt.datetime.fromisoformat("2021-12-02 08:14:48").date()
            plt.title(f"Plot {plot_number}\n{date}")
        color = "darkred" if sex == "A" else "darkblue"
        ax.bar(x=x, height=100 * hist, color=color)
        ax.yaxis.set_major_formatter(mtick.PercentFormatter())
        ax.set_xlabel("BBCH growth stage")
        st.pyplot(fig)
